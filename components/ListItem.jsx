import {Platform, StyleSheet, Text, View} from 'react-native'
import {TouchableOpacity} from 'react-native-gesture-handler'
import Animated, {
    Extrapolation,
    interpolate,
    useAnimatedStyle,
} from 'react-native-reanimated'

export function ListItem({image, scrollY, index}) {
    const imgAnimStyle = useAnimatedStyle(() => {
        const height = interpolate(
            scrollY.value,
            [index * IMAGE_SIZE.MAX, index * IMAGE_SIZE.MAX - IMAGE_SIZE.MAX],
            [IMAGE_SIZE.MAX, IMAGE_SIZE.MIN],
            Extrapolation.CLAMP
        )
        return {
            height,
        }
    })

    const textAnimStyle = useAnimatedStyle(() => {
        const fontSize = interpolate(
            scrollY.value,
            [index * IMAGE_SIZE.MAX, index * IMAGE_SIZE.MAX - IMAGE_SIZE.MAX],
            [TITLE_FONT_SIZE.MAX, TITLE_FONT_SIZE.MIN],
            Extrapolation.CLAMP
        )
        const opacity = interpolate(
            scrollY.value,
            [
                index * IMAGE_SIZE.MAX,
                index * IMAGE_SIZE.MAX - IMAGE_SIZE.MAX / 2,
            ],
            [1, 0],
            Extrapolation.CLAMP
        )
        return {
            fontSize,
            opacity,
        }
    })

    return (
        <TouchableOpacity onPress={() => alert('You clicked!')}>
            <Animated.Image
                source={image.picture}
                style={[styles.image, imgAnimStyle]}
            />
            <View style={styles.textContainer}>
                <Text style={styles.subtitle}>{image.subtitle}</Text>
                <Animated.Text style={[styles.title, textAnimStyle]}>
                    {image.title}
                </Animated.Text>
            </View>
        </TouchableOpacity>
    )
}

export const IMAGE_SIZE = {
    MAX: 300,
    MIN: 100,
}
const TITLE_FONT_SIZE = {
    MAX: 30,
    // iOS does not allow fontSize 0
    MIN: Platform.OS === 'ios' ? 0.41 : 0,
}
const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: IMAGE_SIZE.MAX,
    },
    textContainer: {
        padding: 10,
        backgroundColor: '#0000003a',
        //flex: 1, // flex not working with position: absolute
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
    },
    title: {
        textAlign: 'center', // keep the text centered when it wraps on new line
        fontSize: TITLE_FONT_SIZE.MIN,
        color: 'white',
    },
    subtitle: {
        textAlign: 'center', // keep the text centered when it wraps on new line
        fontSize: 20,
        color: 'white',
    },
})
