import {Dimensions, StyleSheet} from 'react-native'
import Animated, {
    useAnimatedScrollHandler,
    useSharedValue,
} from 'react-native-reanimated'
import {IMAGES} from '../constant'
import {IMAGE_SIZE, ListItem} from './ListItem'

const SCREEN_H = Dimensions.get('screen').height

export function List() {
    const scrollY = useSharedValue(0)
    const scrollHandler = useAnimatedScrollHandler({
        onScroll: e => {
            scrollY.value = e.contentOffset.y
        },
    })
    return (
        // scrollEventThrottle prevents the scroll event to be fired too many times
        <Animated.ScrollView
            contentContainerStyle={{
                height:
                    IMAGES.length * IMAGE_SIZE.MAX +
                    (SCREEN_H - IMAGE_SIZE.MAX),
            }}
            scrollEventThrottle={16}
            onScroll={scrollHandler}
            snapToInterval={IMAGE_SIZE.MAX}
            decelerationRate={'fast'}
            style={styles.scrollview}
        >
            {IMAGES.map((image, i) => (
                <ListItem
                    image={image}
                    key={image.title + i}
                    scrollY={scrollY}
                    index={i}
                />
            ))}
        </Animated.ScrollView>
    )
}

const styles = StyleSheet.create({
    scrollview: {
        backgroundColor: 'black',
    },
})
