import {Dimensions} from 'react-native'
import {GestureHandlerRootView} from 'react-native-gesture-handler'
import {List} from './components/List'

const {width: SCREEN_W, height: SCREEN_H} = Dimensions.get('screen')

export default function App() {
    return (
        <GestureHandlerRootView>
            <List />
        </GestureHandlerRootView>
    )
}
